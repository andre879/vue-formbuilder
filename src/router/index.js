import Vue from 'vue';
import Router from 'vue-router';
import Main from '@/components/Main';
import Options from '@/components/options/Options';
import FormDetails from '@/components/options/FormDetails';
import FormFields from '@/components/options/FormFields';
import FormStyles from '@/components/options/FormStyles';
import Builder from '@/components/builder/Builder';

Vue.use(Router);

Vue.component('options', Options);
Vue.component('form-details', FormDetails);
Vue.component('form-fields', FormFields);
Vue.component('form-styles', FormStyles);
Vue.component('builder', Builder);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
    },
  ],
});
